import axios from 'axios';
import {BASE_URL, TOKEN} from './url';

const API = async (
  url,
  options = {
    method: 'GET',
    body: {},
    head: {},
  },
) => {
  const request = {
    baseURL: BASE_URL,
    method: options.method,
    timeout: 10000,
    url,
    headers: options.head,
    responseType: 'json',
  };
  if (
    request.method === 'POST' ||
    request.method === 'PUT' ||
    request.method === 'DELETE'
  ) {
    request.data = options.body;
  }
  const res = await axios(request);
  if (res.status === 200) {
    return res.data;
  } else {
    return res;
  }
};
export default {
  getDataMobil: async () => {
    return API('mobil', {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    })
      .then(response => {
        return response;
      })
      .catch(err => {
        return err;
      });
  },
  
  postData: async (data) => {
    return API('mobil', {
      method: 'POST',
      body: data,
      head: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    })
      .then(response => {
        
        return response;
      })
      .catch(err => {
        return err;
      });
  },
  editData: async (data) => {
    console.log('daata log', data)
    return API('mobil', {
      method: 'PUT',
      body: data,
      head: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    })
      .then(response => {
        console.log('edit data terakhir',response)
        return response;
      })
      .catch(err => {
        console.log('err', err)
        return err;
      });
  },
  deleteData: async (data) => {
    return API('mobil', {
      method: 'DELETE',
      body: data,
      head: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    })
      .then(response => {
        
        return response;
      })
      .catch(err => {
        return err;
      });
  },

};
