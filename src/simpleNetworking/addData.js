import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {BASE_URL, TOKEN} from '../utils/service/url';
import Modal from "react-native-modal";
import Axios from 'axios'
import API from '../utils/service/apiProvider';


const AddData = ({navigation, route}) => {
  const postData = async () => {
    const body = [
      {
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRl68B_jp4yJEuMe64zZZK6RX-bkMzlLlJVkRGojrm3&s',
      },
    ];
    const response = await API.postData(body);
    navigation.navigate('Home')
    alert('berhasil di tambahakan')
  };
    const editData = async () => {
      const body = [
        {
          _uuid: dataMobil._uuid,
          title: namaMobil,
          harga: hargaMobil,
          totalKM: totalKM,
          unitImage:
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRl68B_jp4yJEuMe64zZZK6RX-bkMzlLlJVkRGojrm3&s',
        },
      ];
      const response = await API.editData(body);
      console.log('edit',response)
      if(response?.items?.length){
        navigation.navigate('Home')
        alert('berhasil di edit')
      }
      else{
        alert('gagal')
    
      }
    }  

  
    const deleteData = async () => {
        const body = [
         {
           "_uuid": dataMobil._uuid,
         }
        ]
        const response = await API.deleteData(body);
        navigation.navigate('Home')
        alert('berhasil di hapus')
        }
      
  

  useEffect(() => {
    if (route.params) {
      // const data = route.params;
      setNamaMobil(route.params.title);
      setTotalKM(route.params.totalKM);
      setHargaMobil(route.params.harga);
    }
  }, []);
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');
  var dataMobil = route.params;
  const [isNamaMobilValid, setIsNamaMobilValid] = useState(true);
  const [isTotalKMValid, setIsTotalKMValid] = useState(true);
  const [isHargaMobilValid, setIsHargaMobilValid] = useState(true);

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <Modal animationType = {"slide"} transparent = {true} isVisible={true} >
        <View style={{backgroundColor:'white', height:'auto', width:'auto'}}>

      <View
        style={{
          
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{
            width: '10%',
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10,
          }}>
          <Icon name="arrowleft" size={20} color="#000" />
        </TouchableOpacity>
        <Text style={{fontSize: 16, fontWeight: 'bold', color: '#000'}}>
          {dataMobil ? 'Ubah data' : 'Tambah data'}
        </Text>
      </View>
      <View
        style={{
          width: '100%',
          padding: 15,
        }}>
        <View>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Nama Mobil
          </Text>
          <TextInput
            placeholder="Masukkan Nama Mobil"
            style={[styles.txtInput, !isNamaMobilValid && styles.invalidInput]}
            value={namaMobil}
            onChangeText={text => setNamaMobil(text)}
            onBlur={() => setIsNamaMobilValid(!!namaMobil)}
          />
        </View>
        <View style={{marginTop: 20}}>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Total Kilometer
          </Text>
          <TextInput
            placeholder="contoh: 100 KM"
            style={[styles.txtInput, !isTotalKMValid && styles.invalidInput]}
            onChangeText={text => setTotalKM(text)}
            value={totalKM}
            onBlur={() => setIsTotalKMValid(!!totalKM)}
          />
        </View>
        <View style={{marginTop: 20}}>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Harga Mobil
          </Text>
          <TextInput
            placeholder="Masukkan Harga Mobil"
            style={[styles.txtInput, !isHargaMobilValid && styles.invalidInput]}
            keyboardType="number-pad"
            onChangeText={text => setHargaMobil(text)}
            value={hargaMobil}
            onBlur={() => setIsHargaMobilValid(!!hargaMobil)}
          />
        </View>
        <TouchableOpacity
          style={styles.btnAdd}
          onPress={() => {
            if (namaMobil && totalKM && hargaMobil) {
              dataMobil ? editData() : postData();
            } else {
              setIsNamaMobilValid(!!namaMobil);
              setIsTotalKMValid(!!totalKM);
              setIsHargaMobilValid(!!hargaMobil);
            }
          }}>
          <Text style={{color: '#fff', fontWeight: '600'}}>
            {dataMobil ? 'Ubah data' : 'Tambah data'}
          </Text>
        </TouchableOpacity>
        { dataMobil ?
            <TouchableOpacity style={[styles.btnAdd, {backgroundColor: 'red'}]} onPress={deleteData}>
          <Text style={{color: '#fff', fontWeight: '600'}}>Hapus Data</Text>
          
        </TouchableOpacity>
        :null
        } 
      </View>
      </View>
      </Modal>

    </View>
  );
};

const styles = StyleSheet.create({
  invalidInput: {
    borderColor: 'red',
  },
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
});

export default AddData;
