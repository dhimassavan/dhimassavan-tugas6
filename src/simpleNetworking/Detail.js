import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  Modal,
  RefreshControl,
  ScrollView,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {BASE_URL, TOKEN} from '../utils/service/url';
import {useIsFocused} from '@react-navigation/native';
import Axios from 'axios';
import API from '../utils/service/apiProvider';
import {convertCurrency} from '../utils/helpers';
import Carousel from 'react-native-snap-carousel';

const Detail = ({navigation, route}) => {
  var dataDetail = route.params;
  console.log('data detail', dataDetail);
  const [dataMobil, setDataMobil] = useState({});
  const isFocused = useIsFocused();

  useEffect(() => {
    getDataMobil();
  }, [isFocused]);
  const getDataMobil = async () => {
    
    const response = await API.getDataMobil();
    console.log('Ontrak LOG: data', response);
    if (response?.items?.length) {
      setDataMobil(response.items);
    }
  };

  return (
    <View style={{flex: 1}}>
      <Image
        source={{uri: dataDetail.unitImage}}
        style={{width: '100%', height: 316, alignItems: 'flex-end'}}>
        </Image>
        <View style={{backgroundColor:'#d7dbd9', borderRadius:20, height:'100%', marginTop:-20}}>
      <View style={{alignItems: 'center', marginTop: 10 }}>
        <Text style={{fontWeight: 'bold', fontSize: 24}}>
          Nama Mobil : {dataDetail.title}
        </Text>
        <Text style={{fontWeight: 'bold', fontSize: 24}}>
          Harga Mobil : {convertCurrency(dataDetail.harga, 'RP. ')}
        </Text>
        <Text style={{fontWeight: 'bold', fontSize: 24}}>
          Jumlah KM : {dataDetail.totalKM}
        </Text>
      </View>
      <View style={{paddingHorizontal: 20}}>
        <TouchableOpacity
          onPress={() => navigation.navigate('AddData', dataDetail)}
          style={styles.btnAdd}>
          <Text style={{color: '#fff', fontWeight: '600'}}>Ubah Data</Text>
        </TouchableOpacity>
      </View>

      <Carousel
        data={dataMobil}
        renderItem={({item, index}) => (
          <TouchableOpacity
            onPress={() => navigation.navigate('Detail', item)}
            activeOpacity={0.8}
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: 15,
              borderColor: 'blue',
              borderWidth: 1,
              borderRadius: 6,
              padding: 12,
              backgroundColor: 'white',
            }}>
            <Image
              style={{
                width: '90%',
                height: 100,
                resizeMode: 'contain',
                alignItems: 'center',
                alignSelf: 'center',
              }}
              source={{uri: item.unitImage}}
            />
            <View
              style={{
                width: '30%',
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              }}></View>
            <View
              style={{
                width: '70%',
                paddingHorizontal: 10,
              }}>
              <View
                style={{
                  width: '70%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Nama Mobil :
                </Text>
                <Text style={{width: '100%', fontSize: 14, color: '#000'}}>
                  {' '}
                  {item.title}
                </Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Total KM :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}>
                  {' '}
                  {item.totalKM}
                </Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Harga Mobil :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}>
                  {' '}
                  {convertCurrency(item.harga, 'Rp. ')}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
        sliderWidth={390} // Sesuaikan lebar carousel sesuai kebutuhan
        itemWidth={280} // Sesuaikan lebar item carousel sesuai kebutuhan
      />
        </View>

    </View>
  );
};
const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
});
export default Detail;
